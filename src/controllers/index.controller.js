const { Pool } = require('pg');

const pool = new Pool({
    host: '3.145.194.87',
    user: 'admin',
    password: 'root',
    database: 'turismo',
    port: '5432'
});

// Category: Tourist Attractions
const getTouristAttractions = async (req, res) => {
    const response = await pool.query('SELECT * FROM cat_atractivo_turistico');
    res.status(200).json(response.rows);
}

const postTouristAttractions = async (req, res) => {
    const { atr_id_tipo, 
        atr_nombre_tipo, 
        atr_nombre_tipo_ingles, 
        atr_image_tipo, 
        atr_fecha } = req.body;

    const response = await pool.query('INSERT INTO cat_atractivo_turistico (atr_id_tipo, atr_nombre_tipo, atr_nombre_tipo_ingles, atr_image_tipo, atr_fecha) VALUES ($1, $2, $3, $4, $5)', [atr_id_tipo, atr_nombre_tipo, atr_nombre_tipo_ingles, atr_image_tipo, atr_fecha]);
    res.json({
        message: 'Type of Tourist Attraction Added Successfully.',
        body: {
            user: {atr_id_tipo, atr_nombre_tipo}
        }
    })
}

// Subcategory: Tourist Attraction 
const getTouristAttractionByIdType = async (req, res) => {
    const atr_id_tipo = req.params.id;
    const response = await pool.query('SELECT * FROM atractivo_turistico WHERE atr_id_tipo = $1', [atr_id_tipo]);    
    res.status(200).json(response.rows);
}

const postTouristAttraction = async (req, res) => {
    const { at_id, atr_id_tipo, at_nombre, at_descripcion_corta, 
        at_descripcion_larga, at_longitud, at_latitud, at_telefono, 
        at_email, at_direccion, at_fecha, at_comollegar, at_red_social, 
        at_red_social_instagram, at_red_social_twitter, at_img_atractivo, 
        at_video_atractivo, at_rating, at_estado, at_nombre_ingles, 
        at_descripcion_corta_ingles } = req.body;

    const response = await pool.query(`INSERT INTO atractivo_turistico 
            (at_id, atr_id_tipo, at_nombre, at_descripcion_corta, at_descripcion_larga, at_longitud,
            at_latitud, at_telefono, at_email, at_direccion, at_fecha, at_comollegar, at_red_social, 
            at_red_social_instagram, at_red_social_twitter, at_img_atractivo, at_video_atractivo, 
            at_rating, at_estado, at_nombre_ingles, at_descripcion_corta_ingles) 
            VALUES ($1, $2, $3, $4, $5,$6, $7, $8, $9, $10,$11, $12, $13, $14, $15,$16, 
            $17, $18, $19, $20,$21)`, 
            [at_id, atr_id_tipo, at_nombre, at_descripcion_corta, 
            at_descripcion_larga, at_longitud, at_latitud, at_telefono, 
            at_email, at_direccion, at_fecha, at_comollegar, at_red_social, 
            at_red_social_instagram, at_red_social_twitter, at_img_atractivo, 
            at_video_atractivo, at_rating, at_estado, at_nombre_ingles, 
            at_descripcion_corta_ingles]);
    res.json({
        message: `Tourist Attraction Added to the category N°: ${atr_id_tipo}`,
        body: {
            user: {at_id, at_nombre, at_descripcion_corta}
        }
    })
}

module.exports = {
    getTouristAttractions, 
    postTouristAttractions, 
    getTouristAttractionByIdType, 
    postTouristAttraction
}

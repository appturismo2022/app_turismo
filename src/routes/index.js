const { Router } = require('express');
const router = Router();
const { getTouristAttractions, postTouristAttractions, 
    getTouristAttractionByIdType, postTouristAttraction } = require('../controllers/index.controller');

router.get('/getTouristAttractions', getTouristAttractions);
router.post('/postTouristAttractions', postTouristAttractions);
router.get('/getTouristAttractionByIdType/:id', getTouristAttractionByIdType);
router.post('/postTouristAttraction', postTouristAttraction);

module.exports = router;